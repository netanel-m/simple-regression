#!/usr/bin/python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt

from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeRegressor 

from sklearn.model_selection import train_test_split

raw_data = pd.read_csv("AAPL_data.csv", sep=",")
raw_data['Epoch'] = (pd.to_datetime(raw_data['Date']) - dt.datetime(1970,1,1)).dt.total_seconds()
data = raw_data[["Epoch"]].to_numpy()
target = raw_data["Open"].to_numpy()

X = data
y = target

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

#reg = LinearRegression()
#reg = KNeighborsRegressor()
#reg = Ridge(alpha=0.5, normalize=True)
#reg = Lasso(normalize=True)
reg = DecisionTreeRegressor()

# fit the model using the training data and training targets
reg.fit(X_train, y_train)

# Predict the price at a given date given in epoch time
print("Test set predictions:\n", reg.predict([[1615558110]]))
print("Training set score: {:.2f}".format(reg.score(X_train, y_train)))
print("Test set score: {:.2f}".format(reg.score(X_test, y_test)))
